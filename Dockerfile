FROM node:12.20.1

# create app directory
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

# copy application source
COPY . /usr/src/app

# install dependencies
RUN npm ci
# build application
RUN npm run build

#set environment variables
ENV IP=0.0.0.0
ENV PORT=8080

EXPOSE 8080
CMD [ "npm", "run", "prod" ]
