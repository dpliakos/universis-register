import { Component, OnInit } from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import { ActivatedUser, LoadingService } from '@universis/common';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  constructor(private _context: AngularDataContext,
              private _activatedUser: ActivatedUser,
              private _loading: LoadingService) { }

  public items: any[] = [];
  public loading = true;

  ngOnInit() {
    this._activatedUser.user.subscribe((user) => {
      if (user) {
        this._loading.showLoading();
        this._context.model('StudyProgramRegisterActions')
          .where('owner').equal(user.id)
          .orderBy('dateModified desc')
          .expand('actionStatus', 'studyProgram', 'specialization', 'inscriptionYear', 'inscriptionPeriod').getItems()
          .then((items) => {
            this.items = items;
            this.loading = false;
            this._loading.hideLoading();
          }).catch((err) => {
            this.loading = false;
            this._loading.hideLoading();
          });
      }
    });
  }

}
